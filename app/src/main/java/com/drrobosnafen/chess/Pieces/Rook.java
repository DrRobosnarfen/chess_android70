package com.drrobosnafen.chess.Pieces;


import android.content.Context;
import android.graphics.BitmapFactory;

import com.drrobosnafen.chess.Game.*;
import com.drrobosnafen.chess.GameView;
import com.drrobosnafen.chess.R;

public class Rook extends Piece{
    public Rook(Context context, boolean colour) {
        super(context, colour);
        myType = 'R';
        if (colour)
            image = BitmapFactory.decodeResource(context.getResources(), R.drawable.rook1);
        else
            image = BitmapFactory.decodeResource(context.getResources(), R.drawable.rook2);
    }
    @Override
    public boolean randomMove(GameView gb){
        int axis = (int) Math.random() * 2;
        int tile = (int) Math.random() * 8;
        if(axis==0){
            return movePiece(gb.getTile(this), gb.getTile(x, tile), gb, true);
        } else {
            return movePiece(gb.getTile(this), gb.getTile(tile, y), gb, true);
        }
    }
    @Override
    public boolean movePiece(Tile start, Tile dest, GameView gb, boolean real){
        if(!super.movePiece(start, dest, gb, real))
            return false;
        //Illegal move: not moving in a horizontal or vertical line
        if(!checkCardinal(dest.getX(), dest.getY(), gb))
            return false;
        if(real) {
            Piece enemyKing = gb.whiteKing;
            Piece myKing = gb.blackKing;
            if (isWhite) {
                enemyKing = gb.blackKing;
                myKing = gb.whiteKing;
            }
            start.updateTile(null);
            if(gb.threatenedTile(gb.getTile(myKing), !isWhite)){
                start.updateTile(this);
                return false;
            }
            if (dest.getPiece() != null){
                decrementPieces(dest.getPiece(), gb);
                if(dest.getPiece()==gb.checkingWhite[0]){
                    gb.checkingWhite[0] = null;
                } else if(dest.getPiece()==gb.checkingBlack[0]){
                    gb.checkingBlack[0] = null;
                }
            }
            set_xy(dest.getX(), dest.getY());
            dest.updateTile(this);
            if(gb.threatenedTile(gb.getTile(enemyKing), isWhite)){
                gb.setCheck(isWhite);
            }
        }

        return true;
    }
}