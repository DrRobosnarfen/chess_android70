package com.drrobosnafen.chess.Pieces;


import android.content.Context;
import android.graphics.BitmapFactory;

import com.drrobosnafen.chess.Game.*;
import com.drrobosnafen.chess.GameView;
import com.drrobosnafen.chess.R;

public class Knight extends Piece{
    public Knight(Context context, boolean colour) {
        super(context, colour);
        myType = 'N';
        if (colour)
            image = BitmapFactory.decodeResource(context.getResources(), R.drawable.knight1);
        else
            image = BitmapFactory.decodeResource(context.getResources(), R.drawable.knight2);
    }
    @Override
    public boolean randomMove(GameView gb){
        int rand = (int) Math.random() * 8;
        switch (rand){
            case 0:
                return movePiece(gb.getTile(this), gb.getTile(x+1,x+2), gb, true);
            case 1:
                return movePiece(gb.getTile(this), gb.getTile(x+-1,x+2), gb, true);
            case 2:
                return movePiece(gb.getTile(this), gb.getTile(x+1,x-2), gb, true);
            case 3:
                return movePiece(gb.getTile(this), gb.getTile(x-1,x-2), gb, true);
            case 4:
                return movePiece(gb.getTile(this), gb.getTile(x+2,x+2), gb, true);
            case 5:
                return movePiece(gb.getTile(this), gb.getTile(x+2,x-2), gb, true);
            case 6:
                return movePiece(gb.getTile(this), gb.getTile(x-2,x+2), gb, true);
            default:
                return movePiece(gb.getTile(this), gb.getTile(x-2,x-2), gb, true);
        }
    }
    @Override
    public boolean movePiece(Tile start, Tile dest, GameView gb, boolean real){
        if(!super.movePiece(start, dest, gb, real))
            return false;
        int dx = dest.getX();
        int dy = dest.getY();
        if (!(Math.abs(dx - x) == 1 && Math.abs(dy - y) == 2) && !(Math.abs(dx - x) == 2 && Math.abs(dy - y) == 1))
            return false;
        if(real) {
            Piece enemyKing = gb.whiteKing;
            Piece myKing = gb.blackKing;
            if (isWhite) {
                enemyKing = gb.blackKing;
                myKing = gb.whiteKing;
            }
            start.updateTile(null);
            if(gb.threatenedTile(gb.getTile(myKing), !isWhite)){
                start.updateTile(this);
                return false;
            }
            if (dest.getPiece() != null){
                decrementPieces(dest.getPiece(), gb);
                if(dest.getPiece()==gb.checkingWhite[0]){
                    gb.checkingWhite[0] = null;
                } else if(dest.getPiece()==gb.checkingBlack[0]){
                    gb.checkingBlack[0] = null;
                }
            }
            set_xy(dx, dy);
            dest.updateTile(this);
            if(gb.threatenedTile(gb.getTile(enemyKing), isWhite)){
                gb.setCheck(isWhite);
            }
        }
        return true;
    }
}
