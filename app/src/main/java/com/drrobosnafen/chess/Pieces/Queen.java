package com.drrobosnafen.chess.Pieces;


import android.content.Context;
import android.graphics.BitmapFactory;

import com.drrobosnafen.chess.Game.*;
import com.drrobosnafen.chess.GameView;
import com.drrobosnafen.chess.R;

public class Queen extends Piece{
    public Queen(Context context, boolean colour) {
        super(context, colour);
        myType = 'Q';
        if (colour)
            image = BitmapFactory.decodeResource(context.getResources(), R.drawable.queen1);
        else
            image = BitmapFactory.decodeResource(context.getResources(), R.drawable.queen2);
    }
    @Override
    public boolean randomMove(GameView gb){
        int dx = (int) Math.random() * 8;
        int dy = (int) Math.random() * 8;
        return movePiece(gb.getTile(this), gb.getTile(dx,dy), gb, true);
    }
    @Override
    public boolean movePiece(Tile start, Tile dest, GameView gb, boolean real){
        if(!super.movePiece(start, dest, gb, real))
            return false;
        int dx = dest.getX();
        int dy = dest.getY();
        //Illegal move: not diagonal, horizontal, or vertical
        if(!checkCardinal(dx, dy, gb) && !checkDiagonal(dx, dy, gb))
            return false;
        if(real) {
            Piece enemyKing = gb.whiteKing;
            Piece myKing = gb.blackKing;
            if (isWhite) {
                enemyKing = gb.blackKing;
                myKing = gb.whiteKing;
            }
            start.updateTile(null);
            if(gb.threatenedTile(gb.getTile(myKing), !isWhite)){
                start.updateTile(this);
                return false;
            }
            if (dest.getPiece() != null){
                decrementPieces(dest.getPiece(), gb);
                if(dest.getPiece()==gb.checkingWhite[0]){
                    gb.checkingWhite[0] = null;
                } else if(dest.getPiece()==gb.checkingBlack[0]){
                    gb.checkingBlack[0] = null;
                }
            }
            set_xy(dx, dy);
            dest.updateTile(this);
            if(gb.threatenedTile(gb.getTile(enemyKing), isWhite)){
                gb.setCheck(isWhite);
            }
        }
        return true;
    }
}
