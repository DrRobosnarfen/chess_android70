package com.drrobosnafen.chess.Pieces;


import android.content.Context;
import android.graphics.BitmapFactory;

import com.drrobosnafen.chess.Game.*;
import com.drrobosnafen.chess.GameView;
import com.drrobosnafen.chess.R;

public class Pawn extends Piece {
    public Context context;
    public Pawn(Context context, boolean colour) {
        super(context, colour);
        myType = 'P';
        this.context = context;
        if (colour)
            image = BitmapFactory.decodeResource(context.getResources(), R.drawable.pawn1);
        else
            image = BitmapFactory.decodeResource(context.getResources(), R.drawable.pawn2);
    }
    @Override
    public boolean randomMove(GameView gb){
        int dx = (int) Math.random() * 3 -1;
        int dy = (int) Math.random()* 5 -2;
        return movePiece(gb.getTile(this), gb.getTile(x+dx,x+dy), gb, true);
    }
    @Override
    public boolean movePiece(Tile start, Tile dest, GameView gb, boolean real){
        if(!super.movePiece(start, dest, gb, real))
            return false;
        int dx = dest.getX();   //destination x
        int dy = dest.getY();   //destination y
        //Illegal move: attempting to move the pawn in the wrong direction
        if((isWhite && (dy-y)>0) || (!isWhite && (dy-y)<0))
            return false;
        //Illegal move: not a diagonal take or greater than 2 forward moves
        if(Math.abs(x-dx)>1 || Math.abs(y-dy)>2){
            return false;
        }
        //Illegal move: attempting to move greater than space forward past the initial move
        if(hasMoved>0){
            if(Math.abs(y-dy)!= 1)
                return false;
        }
        //Illegal move: pawn attempting forward move onto an occupied square
        if(x==dx){
            if(dest.getPiece()!=null)
                return false;
        //If pawn is attempting to move diagonally, and EN_PASSANT_FLAG is set
        } else if(dest.getPiece()==null && gb.EN_PASSANT_FLAG==1){
            //if En Passant X coordinate equals attempted destination's x coordinate
            if(gb.EP_Pawn.getX() == dx && gb.EN_PASSANT_FLAG == 1) {
                decrementPieces(gb.EP_Pawn.getPiece(), gb);
                gb.EP_Pawn.updateTile(null);
            }else
                return false;
        }
        if(Math.abs(y-dy)==2) {
            gb.EN_PASSANT_FLAG = 2;
            gb.EP_Pawn = gb.getTile(dx, dy);
        }
        if(real) {
            Piece enemyKing = gb.whiteKing;
            Piece myKing = gb.blackKing;
            if (isWhite) {
                enemyKing = gb.blackKing;
                myKing = gb.whiteKing;
            }
            start.updateTile(null);
            if(gb.threatenedTile(gb.getTile(myKing), !isWhite)){
                start.updateTile(this);
                return false;
            }
            if (dest.getPiece() != null){
                decrementPieces(dest.getPiece(), gb);
                if(dest.getPiece()==gb.checkingWhite[0]){
                    gb.checkingWhite[0] = null;
                } else if(dest.getPiece()==gb.checkingBlack[0]){
                    gb.checkingBlack[0] = null;
                }
            }
            dest.updateTile(this);
            set_xy(dx, dy);
            if (dy == 0 || dy == 7) {
                Piece promo = new Queen(context, isWhite);
                switch (gb.Promotion_class) {
                    case 'R':
                        promo = new Rook(context, isWhite);
                        break;
                    case 'B':
                        promo = new Bishop(context, isWhite);
                        break;
                    case 'N':
                        promo = new Knight(context, isWhite);
                        break;
                    case 'Q':
                        break;
                }
                gb.Promotion_class = 'Q';
                promo.set_xy(dx, dy);
                dest.updateTile(promo);
            }
            if(gb.threatenedTile(gb.getTile(enemyKing), isWhite)){
                gb.setCheck(isWhite);
            }
        }
        return true;
    }
}