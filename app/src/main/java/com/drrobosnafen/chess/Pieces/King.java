package com.drrobosnafen.chess.Pieces;


import android.content.Context;
import android.graphics.BitmapFactory;
import com.drrobosnafen.chess.Game.*;
import com.drrobosnafen.chess.GameView;
import com.drrobosnafen.chess.R;

public class King extends Piece {
    public King(Context context, boolean colour) {
        super(context, colour);
        myType = 'K';
        if (colour)
            image = BitmapFactory.decodeResource(context.getResources(), R.drawable.king1);
        else
            image = BitmapFactory.decodeResource(context.getResources(), R.drawable.king2);
    }

    @Override
    public boolean randomMove(GameView gb){
        int rand = (int) Math.random() * 3 -1;
        return movePiece(gb.getTile(this), gb.getTile(x+rand,x+rand), gb, true);
    }
    @Override
    public boolean movePiece(Tile start, Tile dest, GameView gb, boolean real){
        if(!super.movePiece(start, dest, gb, real))
            return false;
        int dx = dest.getX();   //destination x
        int dy = dest.getY();   //destination y
        if(Math.abs(gb.blackKing.x-gb.whiteKing.x)==1 && Math.abs(gb.blackKing.y-gb.whiteKing.y)==1)
            return false;
        if(gb.threatenedTile(dest, !isWhite))
            return false;
        if(Math.abs(x-dx)>1 || Math.abs(y-dy)>1) {
            if(Math.abs(x-dx)==2){
                if(hasMoved>0 || (isWhite&&gb.whiteCheck) || (!isWhite&&gb.blackCheck))
                    return false;
                if(dx==6){
                    if(gb.getTile(5, dy).getPiece()!= null || gb.getTile(6, dy).getPiece()!=null || gb.getTile(7, dy).getPiece()==null)
                        return false;
                    else if(gb.getTile(7,dy).getPiece().myType!='R' || gb.getTile(7,dy).getPiece().hasMoved >0)
                        return false;
                    if(gb.threatenedTile(gb.getTile(5,dy), !isWhite) || gb.threatenedTile(gb.getTile(6, dy), !isWhite))
                        return false;
                    gb.getTile(7,dy).getPiece().movePiece(gb.getTile(7,dy), gb.getTile(5,dy), gb, true);
                } else if(dx==2){
                    if(gb.getTile(3, dy).getPiece()!= null || gb.getTile(2, dy).getPiece()!=null || gb.getTile(0, dy).getPiece()==null)
                        return false;
                    else if(gb.getTile(0,dy).getPiece().myType!='R' || gb.getTile(0,dy).getPiece().hasMoved>0)
                        return false;
                    if(gb.threatenedTile(gb.getTile(3, dy), !isWhite) || gb.threatenedTile(gb.getTile(2, dy), !isWhite))
                        return false;
                    gb.getTile(0,dy).getPiece().movePiece(gb.getTile(0,dy), gb.getTile(3,dy), gb, true);
                }
            } else
                return false;
        }
        if(real) {
            if(dest.getPiece()!=null)
                decrementPieces(dest.getPiece(), gb);
            set_xy(dest.getX(), dest.getY());
            dest.updateTile(this);
            start.updateTile(null);
            if (gb.blackCheck){
                gb.blackCheck = false;
                gb.checkingBlack[0] = null;
                gb.checkingBlack[1] = null;
            }
            if (gb.whiteCheck){
                gb.whiteCheck = false;
                gb.checkingWhite[0] = null;
                gb.checkingWhite[1] = null;
            }

        }
        return true;
    }
}
