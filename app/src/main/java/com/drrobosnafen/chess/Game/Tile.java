package com.drrobosnafen.chess.Game;


import com.drrobosnafen.chess.Pieces.*;

public class Tile {
    //x, y coordinates to help with math
    private int isTileWhite, x, y;
    private Piece myPiece;
    //empty tile constructor
    public Tile(int colour, int x, int y){
        this.x = x;
        this.y = y;
        myPiece = null;
        isTileWhite = colour;
    }
    //constructor with a chess piece
    public Tile(int colour, int x, int y, Piece p){
        this.x = x;
        this.y = y;
        p.set_xy(x, y);
        myPiece = p;
        isTileWhite = colour;
    }
    //update method for movement / capture
    public void updateTile(Piece newPiece){
        myPiece = newPiece;
    }
    //getters
    public Piece getPiece(){
        return myPiece;
    }
    public int getX(){
        return x;
    }
    public int getY(){
        return y;
    }
}