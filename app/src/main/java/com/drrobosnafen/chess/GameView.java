package com.drrobosnafen.chess;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.Log;
import android.view.DragAndDropPermissions;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import com.drrobosnafen.chess.Game.Tile;
import com.drrobosnafen.chess.Pieces.Bishop;
import com.drrobosnafen.chess.Pieces.King;
import com.drrobosnafen.chess.Pieces.Knight;
import com.drrobosnafen.chess.Pieces.Pawn;
import com.drrobosnafen.chess.Pieces.Piece;
import com.drrobosnafen.chess.Pieces.Queen;
import com.drrobosnafen.chess.Pieces.Rook;

import java.io.Console;
import java.util.ArrayList;
import java.util.Collection;
import java.util.StringTokenizer;


public class GameView extends SurfaceView implements SurfaceHolder.Callback {
    private MainThread thread;
    private Context myContext;
    public static final int screenWidth = Resources.getSystem().getDisplayMetrics().widthPixels;
    public static final int cellDimension = screenWidth / 8;

    private Bitmap background;
    private static ArrayList<Piece> pieceList = new ArrayList<>();

    private ArrayList<ArrayList<String>> recordings = new ArrayList<>();
    private Piece movePiece;
    private boolean skip;
    private boolean HIGHLIGHT_FLAG = false;
    public static boolean COUNTERATTACK_FLAG = false;
    public static boolean whiteCheck, blackCheck;
    public static Piece whiteKing, blackKing;
    public static boolean whiteKing_immobile = true, blackKing_immobile = true;
    public static int remainingWhite = 16, remainingBlack = 16;
    private Tile[][] board = new Tile[8][8];
    public static Tile EP_Pawn;
    public Piece[] checkingWhite = new Piece[2];
    public Piece[] checkingBlack = new Piece[2];
    public static int EN_PASSANT_FLAG = 0;
    public boolean whiteTurn = true, inProgress = true;
    private boolean whiteVictory, blackVictory;
    private boolean DRAW_FLAG = false;
    private boolean acceptDraw = false;
    public static char Promotion_class = 'Q';
    private Piece undoPiece = null;
    private Tile undoTile;
    private boolean showET;
    public Piece  undoCapture = null;
    public GameView(Context context) {
        super(context);
        getHolder().addCallback(this);
        thread = new MainThread(getHolder(), this);
        setFocusable(true);
        myContext = context;
        background = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.chessboard), screenWidth, screenWidth, false);
        populateBoard(myContext);
    }

    @Override
    public boolean onTouchEvent(MotionEvent e) {
        int x = (int) e.getX();
        int y = (int) e.getY();
        if(skip) {
            skip = false;
            return true;
        }
        if (x < screenWidth && y < screenWidth) {
            if(blackVictory || whiteVictory || acceptDraw){
                if(x> cellDimension && x < cellDimension*3 && y> screenWidth/2 && y< screenWidth/2 +120){
                    showET = true;
                    resetState();
                    populateBoard(myContext);
                }
                if(x> cellDimension*5 && x < cellDimension*7 && y> screenWidth/2 && y< screenWidth/2 +120){
                    resetState();
                    populateBoard(myContext);
                }
            }
            x = (int) Math.floor((double) (x / cellDimension));
            y = (int) Math.floor((double) (y / cellDimension));
            if (movePiece == null) {
                movePiece = board[x][y].getPiece();
                HIGHLIGHT_FLAG = true;
            } else {
                attemptMove(x, y);
                HIGHLIGHT_FLAG = false;
                movePiece = null;
            }
        }
        if(y>screenWidth && y < screenWidth + 120){
            if(x< cellDimension*2){
              /*int rand;
                  do {
                      do {
                          rand = (int) Math.random() * (pieceList.size());
                      } while (pieceList.get(rand).isWhite != whiteTurn);
                      movePiece = pieceList.get(rand);
                  }while(!movePiece.randomMove(this));
                movePiece = null; */
            } else if(x>cellDimension*2 && x<cellDimension*4){
                if(undoPiece == null){
                    return true;
                }
                if(undoCapture != null){
                    getTile(undoPiece).updateTile(undoCapture);
                    pieceList.add(undoCapture);
                    if(whiteTurn){
                        remainingWhite++;
                    } else {
                        remainingBlack++;
                    }
                } else{
                    getTile(undoPiece).updateTile(null);
                }
                undoPiece.set_xy(undoTile.getX(), undoTile.getY());
                undoPiece.hasMoved-= 2;
                undoTile.updateTile(undoPiece);
                whiteTurn = !whiteTurn;
                undoTile = null;
                undoPiece = null;
                undoCapture = null;
            }else if(x>cellDimension*4 && x<cellDimension*6){
                if(DRAW_FLAG) {
                    inProgress = false;
                    acceptDraw = true;
                } else
                DRAW_FLAG = true;
            }else if(x>cellDimension*6){
                inProgress = false;
                if(whiteTurn)
                    blackVictory = true;
                else
                    whiteVictory = true;
            }
            return true;
        }
        skip = true;
        return true;
    }

    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
    }
    public void surfaceCreated(SurfaceHolder holder) {
        thread.setRunning(true);
        thread.start();
    }
    public void surfaceDestroyed(SurfaceHolder holder) {
        boolean retry = true;
        while (retry) {
            try {
                thread.setRunning(false);
                thread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            retry = false;
        }
    }


    public Tile getTile(int x, int y){
        return board[x][y];
    }
    public Tile getTile(Piece p){
        return board[p.x][p.y];
    }
    public void removePiece(Piece p){
        pieceList.remove(p);
    }
    public boolean attemptMove(int dx, int dy){
        //start_file, start_rank, dest_file, dest_rank
        int sx = movePiece.x;
        int sy = movePiece.y;
        DRAW_FLAG = false;
        //attempting to move an empty tile
        if(movePiece==null){
            return false;
        }
        if(blackCheck && movePiece!=blackKing && !hasSavior(false))
            return false;
        if(whiteCheck && movePiece!=whiteKing && !hasSavior(true))
            return false;
        //attempting to move the other player's piece
        if((movePiece.isWhite && !whiteTurn) || (!movePiece.isWhite && whiteTurn)){
            return false;
        }
        boolean legal_move = movePiece.movePiece(board[sx][sy], board[dx][dy], this, true);
        if(legal_move) {
            whiteTurn = !whiteTurn;
            undoPiece = movePiece;
            undoTile = getTile(sx, sy);
        }
        determineMobility(whiteTurn);
        if(blackCheck && blackKing_immobile){
              if(!hasSavior(false)){
                    whiteVictory = true;
                    inProgress = false;
              }
        }
        if(whiteCheck && whiteKing_immobile){
               if(!hasSavior(true)){
                    blackVictory = true;
                    inProgress = false;
               }
        }
        if(EN_PASSANT_FLAG>0)
            EN_PASSANT_FLAG--;

        return legal_move;
    }
    public boolean hasSavior(boolean colour){
        Piece attacker;
        int i, j;
        if(colour){
            if(checkingWhite[0]!=null && checkingWhite[1]!=null)
                return false;
            attacker = checkingWhite[0];
            if(attacker.myType == 'N')
                return threatenedTile(getTile(attacker), colour);
            if(attacker.x >= whiteKing.x){
                for(i=attacker.x; i>whiteKing.x; i--) {
                    if (attacker.y >= whiteKing.y) {
                        for (j = attacker.y; j > whiteKing.y; j--) {
                            if (threatenedTile(getTile(i, j), colour))
                                return true;
                        }
                    } else {
                        for (j = attacker.y; j < whiteKing.y; j++) {
                            if (threatenedTile(getTile(i, j), colour))
                                return true;
                        }
                    }
                }
            } else {
                for(i=attacker.x; i<whiteKing.x; i++) {
                    if (attacker.y >= whiteKing.y) {
                        for (j = attacker.y; j > whiteKing.y; j--) {
                            if (threatenedTile(getTile(i, j), colour))
                                return true;
                        }
                    } else {
                        for (j = attacker.y; j < whiteKing.y; j++) {
                            if (threatenedTile(getTile(i, j), colour))
                                return true;
                        }
                    }
                }
            }
        } else{
            if(checkingBlack[0]!=null && checkingBlack[1]!=null)
                return false;
            attacker = checkingBlack[0];
            if(attacker.myType == 'N')
                return threatenedTile(getTile(attacker), !colour);
            if(attacker.x >= blackKing.x){
                for(i=attacker.x; i>blackKing.x; i--) {
                    if (attacker.y >= blackKing.y) {
                        for (j = attacker.y; j > blackKing.y; j--) {
                            if (threatenedTile(getTile(i, j), !colour))
                                return true;
                        }
                    } else {
                        for (j = attacker.y; j < blackKing.y; j++) {
                            if (threatenedTile(getTile(i, j), !colour))
                                return true;
                        }
                    }
                }
            } else {
                for(i=attacker.x; i<blackKing.x; i++) {
                    if (attacker.y >= blackKing.y) {
                        for (j = attacker.y; j > blackKing.y; j--) {
                            if (threatenedTile(getTile(i, j), !colour))
                                return true;
                        }
                    } else {
                        for (j = attacker.y; j < blackKing.y; j++) {
                            if (threatenedTile(getTile(i, j), !colour))
                                return true;
                        }
                    }
                }
            }
        }
        return false;
    }
    public void setCheck(boolean black_in_check){
        if(black_in_check)
            blackCheck = true;
        else
            whiteCheck = true;
    }
    public boolean threatenedTile(Tile target, boolean whiteAggressor){
        int i, j, count;
        Piece occupant;
        boolean isThreatened = false;
        int threateningKing = 0;
        if(target==getTile(blackKing))
            threateningKing = 2;
        else if(target==getTile(whiteKing))
            threateningKing = 4;
        if(whiteAggressor)
            count = remainingWhite;
        else
            count = remainingBlack;
        if(target.getPiece()!=null){
            if(whiteAggressor == target.getPiece().isWhite)
                COUNTERATTACK_FLAG = true;
        }
        for(i=0; i<8; i++){
            for(j=0; j<8; j++){
                occupant = getTile(i,j).getPiece();
                if(occupant!=null){
                    if(whiteAggressor != occupant.isWhite || occupant==blackKing || occupant==whiteKing)
                        continue;
                    else
                        count--;
                    if(occupant.movePiece(getTile(occupant), target, this, false)){
                        if(threateningKing>0){
                            if(threateningKing<3){
                                checkingBlack[threateningKing%2] = occupant;
                                threateningKing--;
                            } else {
                                checkingWhite[threateningKing%2] = occupant;
                                threateningKing--;
                            }
                        }
                        COUNTERATTACK_FLAG = false;
                        isThreatened = true;
                    }
                }
                if(count==0)
                    return isThreatened;
            }
        }
        return isThreatened;
    }
    private void determineMobility(boolean white) {
        int i, j, dx, dy;
        if (white) {
            for (i = -1; i < 2; i++) {
                dx = whiteKing.x + i;
                if (dx < 0 || dx > 7)
                    continue;
                ;
                for (j = -1; j < 2; j++) {
                    dy = whiteKing.y + j;
                    if (dy < 0 || dy > 7)
                        continue;
                    if (whiteKing.movePiece(getTile(whiteKing), getTile(dx, dy), this, false)) {
                        whiteKing_immobile = false;
                        return;
                    }
                }
            }
            whiteKing_immobile = true;
        } else {
            for (i = -1; i < 2; i++) {
                dx = blackKing.x + i;
                if (dx < 0 || dx > 7)
                    continue;
                ;
                for (j = -1; j < 2; j++) {
                    dy = blackKing.y + j;
                    if (dy < 0 || dy > 7)
                        continue;
                    if (blackKing.movePiece(getTile(blackKing), getTile(dx, dy), this, false)) {
                        blackKing_immobile = false;
                        return;
                    }
                }
            }
            blackKing_immobile = true;
        }
    }
    public void draw(Canvas canvas)
    {
        super.draw(canvas);
        Paint p = new Paint();
        canvas.drawBitmap(background, 0, 0, null);
        for(Piece pi: pieceList){
            pi.draw(canvas);
        }
        p.setColor(Color.GRAY);
        canvas.drawRect(0, screenWidth , cellDimension*2-1, screenWidth + 120, p);
        canvas.drawRect(cellDimension*2, screenWidth , cellDimension*4-1, screenWidth + 120, p);
        canvas.drawRect(cellDimension*4, screenWidth , cellDimension*6-1, screenWidth + 120, p);
        canvas.drawRect(cellDimension*6, screenWidth , cellDimension*8, screenWidth + 120, p);
        p.setColor(Color.BLACK);
        p.setTextSize(75);
        p.setStyle(Paint.Style.FILL);
        canvas.drawText("AI", 10, screenWidth+90, p);
        canvas.drawText("Undo", cellDimension*2+10, screenWidth+90, p);
        canvas.drawText("Draw", cellDimension*4+10, screenWidth+90, p);
        p.setTextSize(60);
        canvas.drawText("Resign", cellDimension*6+10, screenWidth+90, p);
        p.setColor(Color.WHITE);
        if(DRAW_FLAG)
            canvas.drawText("Accept Draw?", cellDimension*4, screenWidth + 200, p);
        if(whiteTurn) {
            canvas.drawText("White's Turn", 10, screenWidth + 200, p);
        }
        else {
            canvas.drawText("Black's Turn", 10, screenWidth + 200, p);
        }
        if((whiteCheck && blackVictory) || (blackCheck &&whiteVictory)) {
            canvas.drawText("Checkmate", 10, screenWidth + 280, p);
        } else {
            if (whiteCheck) {
                canvas.drawText("White King in Check", 10, screenWidth + 280, p);
            } else if (blackCheck) {
                canvas.drawText("Black King in Check", 10, screenWidth + 280, p);
            }

        }
        if(!inProgress) {
            if (whiteVictory) {
                canvas.drawText("White Victory", 10, screenWidth + 360, p);
            } else if (blackVictory) {
                canvas.drawText("Black Victory", 10, screenWidth + 360, p);
            } else if (acceptDraw) {
                canvas.drawText("Draw!", 10, screenWidth + 360, p);
            }
            p.setTextSize(70);
            p.setColor(Color.BLACK);
            canvas.drawText("Save Game?", 200, screenWidth/2 - 30, p);
            canvas.drawRect(cellDimension*1, screenWidth/2 , cellDimension*3, screenWidth/2 + 120, p);
            canvas.drawRect(cellDimension*5, screenWidth/2 , cellDimension*7, screenWidth/2 + 120, p);
            p.setColor(Color.WHITE);
            canvas.drawText("Yes", 130, screenWidth/2 + 90, p);
            canvas.drawText("No", 530, screenWidth/2 + 90, p);
        }
        if(HIGHLIGHT_FLAG){
            p.setColor(Color.RED);
            canvas.drawCircle(movePiece.x * cellDimension +50, movePiece.y* cellDimension+50, 10, p);
        }
    }
    private void resetState(){
        pieceList = new ArrayList<>();
        movePiece = null;
        skip = false;
        HIGHLIGHT_FLAG = false;
        COUNTERATTACK_FLAG = false;
        whiteCheck = false;
        blackCheck = false;
        whiteKing_immobile = true;
        blackKing_immobile = true;
        remainingWhite = 16;
        remainingBlack = 16;
        board = new Tile[8][8];
        EP_Pawn = null;
        checkingWhite = new Piece[2];
        checkingBlack = new Piece[2];
        EN_PASSANT_FLAG = 0;
        whiteTurn = true;
        inProgress = true;
        whiteVictory = false;
        blackVictory = false;
        DRAW_FLAG = false;
        acceptDraw = false;
        undoPiece = null;
        undoTile = null;
        undoCapture = null;
    }
    public void populateBoard(Context context){
        for (int i = 0; i < 8; i++) {
            for (int j = 6; j >= 1; j--) {
                int colour = (i + j) % 2;
                if (j == 6) {
                    board[i][j] = new Tile(colour, i, j, new Pawn(context, true));
                    pieceList.add(board[i][j].getPiece());
                } else if (j == 1) {
                    board[i][j] = new Tile(colour, i, j, new Pawn(context, false));
                    pieceList.add(board[i][j].getPiece());
                } else
                    board[i][j] = new Tile(colour, i, j);
            }
        }
        whiteKing = new King(context, true);
        blackKing = new King(context, false);
        board[0][7] = new Tile(1, 0, 7, new Rook(context, true));
        board[1][7] = new Tile(0, 1, 7, new Knight(context, true));
        board[2][7] = new Tile(1, 2, 7, new Bishop(context, true));
        board[3][7] = new Tile(0, 3, 7, new Queen(context, true));
        board[4][7] = new Tile(1, 4, 7, whiteKing);
        board[5][7] = new Tile(0, 5, 7, new Bishop(context, true));
        board[6][7] = new Tile(1, 6, 7, new Knight(context, true));
        board[7][7] = new Tile(0, 7, 7, new Rook(context, true));
        board[0][0] = new Tile(0, 0, 0, new Rook(context, false));
        board[1][0] = new Tile(1, 1, 0, new Knight(context, false));
        board[2][0] = new Tile(0, 2, 0, new Bishop(context, false));
        board[3][0] = new Tile(1, 3, 0, new Queen(context, false));
        board[4][0] = new Tile(0, 4, 0, blackKing);
        board[5][0] = new Tile(1, 5, 0, new Bishop(context, false));
        board[6][0] = new Tile(0, 6, 0, new Knight(context, false));
        board[7][0] = new Tile(1, 7, 0, new Rook(context, false));
        for (int i = 0; i < 8; i++) {
            pieceList.add(board[i][0].getPiece());
            pieceList.add(board[i][7].getPiece());
        }
    }
}
